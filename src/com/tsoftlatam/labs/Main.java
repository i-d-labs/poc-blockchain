package com.tsoftlatam.labs;

import com.tsoftlatam.labs.entity.Block;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {

    private static List<Block> blockchain = new ArrayList<>();

    private static int prefix = 4;

    private static String prefixString = new String(new char[prefix]).replace('\0', '0');

    public static void main(String[] args) {

        Block bloqueGenesis = new Block("Bloque Genesis", "0", new Date().getTime());

        bloqueGenesis.mineBlock(prefix);

        blockchain.add(bloqueGenesis);

        System.out.println("Se agrego el bloque genesis");

        Block nuevoBloque = new Block("Nuevo bloque", blockchain.get(blockchain.size() - 1).getHash(), new Date().getTime());

        nuevoBloque.mineBlock(prefix);

        blockchain.add(nuevoBloque);

        System.out.println("Se agrego el nuevo bloque");

        Block bloqueCorrupto = new Block ("Bloque corrupto", "1", new Date().getTime());

        bloqueCorrupto.mineBlock(prefix);

        blockchain.add(bloqueCorrupto);

        verificarCadena();

    }

    public static void verificarCadena () {

        boolean flag = true;

        for (int i = 0; i < blockchain.size(); i++) {

            String previousHash = i == 0 ? "0"
                    :
                    blockchain.get(i - 1).getHash();

            flag = blockchain.get(i).getHash().equals(blockchain.get(i).calculateBlockHash())
                    && previousHash.equals(blockchain.get(i).getPreviousHash())
                    && blockchain.get(i).getHash().substring(0, prefix).equals(prefixString);

            if (!flag) {

                System.err.println("La cadena de bloques se encuentra corrupta");

                break;

            }

        }

    }

}
